import matplotlib.pyplot as plt
import csv
import os
from pathlib import Path
import matplotlib.dates as mdate

os.chdir(str(Path.home()))
time = []
list_names = []

#Read and store the data in arrays
with open('/tempmon_data/temperatures.txt','r') as csvfile:
    rows = csv.reader(csvfile, delimiter=',')
    count = 0
    for row in rows:
        if count == 2:
            for j in range(1, len(row)-1): #The first element is the timestamp and the last one an empty string
                sensor_name = row[j].lstrip()
                list_names.append(sensor_name)
            print(list_names)
            #Create empty 2-d array to store the sensor data
            sensor_data = []
            for i in range(len(list_names)):
                sensor_data.append([])

        if row[0][0] != '#': #read the lines that are not commented
            time.append(float(row[0]))
            for i in range(len(list_names)): #Go through the temperatures
                temp = float(row[i+1])
                if temp < 80:
                    sensor_data[i].append(temp)
                else:
                    sensor_data[i].append(sensor_data[i][-1]) #append the same value as the previous measure

        count = count + 1


#Plot the data
formated_time = mdate.epoch2num(time)
fig, ax = plt.subplots()

for i in range(len(list_names)):
    plt.plot(formated_time, sensor_data[i], label = list_names[i], lw = 0.5)#linestyle='--', marker='o')

# Choose your xtick format string
date_fmt = '%d-%m-%y %H:%M:%S'
# Use a DateFormatter to set the data to the correct format.
date_formatter = mdate.DateFormatter(date_fmt)
ax.xaxis.set_major_formatter(date_formatter)
# Sets the tick labels diagonal so they fit easier.
fig.autofmt_xdate()

plt.xlabel('time (s)')
plt.ylabel('temperature (Cº)')
plt.title('Lab temperature monitoring')
plt.legend()
plt.savefig('temperature_plot.png')
plt.show()